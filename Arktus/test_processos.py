from acoes.Interacoes import clique
from time import sleep
import steps.cadastro.test_cadastro_com_documento as cadastro
import steps.login.test_login as login
import steps.login.test_login_revenda_autorizada as login_autorizado
import steps.alterar_dados.test_altera_dados as altera_dados
import steps.pedido.test_realiza_pedido as pedido
import navegador.browser as web


def processo_cadastro(pessoa):
    print("Inicio do processo de cadastro..") 
    browser = web.chrome()
    cadastro.realizarCadastro(browser, pessoa)
    web.close(browser)
    sleep(2)
    print("Cadastro relizado com sucesso") 


def processo_login(pessoa):
    print("Inicio do processo de login..") 
    browser = web.chrome()
    login.realizarLogin(browser, pessoa)
    web.close(browser)
    sleep(2)
    print("Login realizado com sucesso") 

def processo_alterar_dados(pessoa):
    print("Inicio do processo de alteração de dados..") 
    browser = web.chrome()
    login.realizarLogin(browser, pessoa)
    sleep(2)
    altera_dados.alteraDados(browser)
    sleep(2)
    print("Alterações realizadas com sucesso") 

def processo_realizar_pedido(pessoa):
    print("Inicio do processo de realização de um pedido..")
    browser = web.chrome()
    login_autorizado.realizarLogin(browser, pessoa)
    sleep(2)
    pedido.pagamento_via_deposito(browser, pessoa)
    sleep(10)
    pedido.pagamento_via_boleto(browser, pessoa)
    sleep(10)
    pedido.pagamento_via_cartao(browser, pessoa)
    sleep(3600)