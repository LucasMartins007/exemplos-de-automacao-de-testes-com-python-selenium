import erros.error_path.error_path_login as error
import emailconfig.send_email as emailConfig
 
def verificaErros(browser, pessoa):
    if (is_erro(browser, pessoa)):
        return False
    else:
        return True


def is_erro(browser, pessoa):
    padrao_msg = 'Erro ao tentar fazer login com '

    if (len(browser.find_elements_by_xpath(error.userNotFound)) > 0):
        message = padrao_msg + '<br>email: ' + pessoa.get('email')
        message = message + '<br>senha: ' + pessoa.get('senha')
        emailConfig.envia_msg_erro('', browser, message)
        return True

    if (len(browser.find_elements_by_xpath(error.campoObrigatorioLogin)) > 0):
        message = padrao_msg + 'login: '
        emailConfig.envia_msg_erro(pessoa.get('email'), browser, message)
        return True
        
    if (len(browser.find_elements_by_xpath(error.campoObrigatorioSenha)) > 0):
        message = padrao_msg + 'senha: '
        emailConfig.envia_msg_erro(pessoa.get('senha'), browser, message)
        return True

    
    
    