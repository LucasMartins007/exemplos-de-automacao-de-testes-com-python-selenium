import erros.error_path.error_path_cadastro as error  
import emailconfig.send_email as emailConfig
import acoes.Interacoes as interacao


def verificaErros(browser, pessoa):
    if (is_erro(browser, pessoa)):
        return False
    else:
        return True


def is_erro(browser, pessoa):
    padrao_msg = 'Erro ao tentar cadastrar o campo '
    if (interacao.elemento_existe(browser, error.erroEmail)):
        message = padrao_msg + 'email: '
        emailConfig.envia_msg_erro(pessoa.get('email'), browser, message)
        return True

    if (interacao.elemento_existe(browser, error.erroSenha)):
        message = padrao_msg + 'senha: '
        emailConfig.envia_msg_erro(pessoa.get('senha'), browser, message)
        return True

    if (interacao.elemento_existe(browser, error.erroDDD)):
        return True

    if (interacao.elemento_existe(browser, error.erroTelefone)):
        message = padrao_msg + 'telefone: '
        emailConfig.envia_msg_erro(pessoa.get('celular'), browser, message)
        return True

    if (interacao.elemento_existe(browser, error.erroResponsavel)):
        message = padrao_msg + 'responsável: '
        return True

    if (interacao.elemento_existe(browser, error.erroRazaoSocial)):
        message = padrao_msg + 'razão social: '
        return True

    if (interacao.elemento_existe(browser, error.erroCNPJ)):
        message = padrao_msg + 'CNPJ: '
        return True
        
    if (interacao.elemento_existe(browser, error.erroInscricaoEstadual)):
        message = padrao_msg + 'inscrição estadual: '
        return True