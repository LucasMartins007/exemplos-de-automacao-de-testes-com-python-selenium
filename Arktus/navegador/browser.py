from selenium import webdriver
import sys
import os

def chrome():
     PATH = "../chromedriver"
     from selenium.webdriver.chrome.options import Options  
     chrome_options = Options()
     chrome_options.add_argument("--headless")
     
     browser = webdriver.Chrome(PATH)
     #browser = webdriver.Chrome(PATH, options=chrome_options) # Abrir onavegador sem mostrar na tela
     url = "https://next.arktus.com.br//"
     browser.get(url)
     
     browser.maximize_window() 
     return browser

def close(browser):
     browser.quit()

def restart():
    python = sys.executable
    os.execl(python, python, * sys.argv)

def refresh(browser):
     browser.refresh()

def get_customer_id(browser):
    cookies_list=browser.get_cookies() 
    for cookie in cookies_list:
           value = cookie['value']
    return value

def scrool_bottom(browser):
     browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

def scrool_to(browser, y):
     browser.execute_script("window.scrollTo(0, " + str(y) + ");")