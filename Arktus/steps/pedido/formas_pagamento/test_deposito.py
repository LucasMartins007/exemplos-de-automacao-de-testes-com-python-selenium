from time import sleep
import paths.xPath_pedido as xPath
import acoes.Interacoes as interacao
import acoes.generator as gerar
import navegador.browser as web

def pagamento_via_deposito(browser):
    interacao.clique(browser, xPath.abrir_card_deposito)
    sleep(1)
    interacao.clique(browser, xPath.bt_finalizar_pagamento_deposito)