from time import sleep
import paths.xPath_pedido as xPath
import acoes.Interacoes as interacao
import acoes.generator as gerar
import navegador.browser as web

def pagamento_via_boleto(browser):
    interacao.clique(browser, xPath.abrir_card_boleto)
    sleep(3)
    interacao.clique(browser, xPath.bt_finalizar_pagamento_boleto)
    sleep(15)
    interacao.clique(browser, xPath.bt_imprimir_boleto)

