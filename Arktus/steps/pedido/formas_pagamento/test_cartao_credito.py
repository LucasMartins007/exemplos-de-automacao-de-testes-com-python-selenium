from time import sleep
import paths.xPath_pedido as xPath
import acoes.Interacoes as interacao
import acoes.generator as gerar
import navegador.browser as web

def pagamento_via_cartao(browser, pessoa):
    parcelamentos = [xPath.select_parcelamento_2x, xPath.select_parcelamento_1x, xPath.select_parcelamento_3x]

    interacao.clique(browser, xPath.abrir_card_ct)
    sleep(1)
    interacao.clique(browser, xPath.bt_adicionar_cartao)
    sleep(1)
    interacao.clique(browser, xPath.bt_remover_cartao)
    sleep(1)
    interacao.preencherCampoEspecifico(browser, xPath.campo_numero_cartao, '5583509691513638', 16)
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_nome_titular, pessoa.get('nome'))
    sleep(1)
    interacao.preencherCampoEspecifico(browser, xPath.campo_data_validade, '062022', 8)
    sleep(1)
    interacao.preencherCampoEspecifico(browser, xPath.campo_cod_seguranca, '123', 3)
    sleep(1)
    interacao.clique(browser, xPath.select_parcelamento)
    sleep(1)
    interacao.selecionarOpcao(browser, xPath.select_parcelamento, parcelamentos, 3)
    sleep(1)
    interacao.clique(browser, xPath.bt_finalizar_pagamento_cartao)


