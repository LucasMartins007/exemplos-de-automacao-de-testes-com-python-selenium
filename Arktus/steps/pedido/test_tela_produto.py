from time import sleep
import paths.xPath_pedido as xPath
import acoes.Interacoes as interacao
import acoes.generator as gerar
import navegador.browser as web

def tela_produto(browser, pessoa):
    interacao.clique(browser, xPath.card_produto)
    sleep(10)
    interacao.clique(browser, xPath.bt_add_carrinho)
    sleep(15)
    interacao.preencherCampoEspecifico(browser, xPath.campo_cep, pessoa.get('cep'), 8)
    sleep(1)
    interacao.clique(browser, xPath.bt_busca_cep)
    sleep(5)
    interacao.clique(browser, xPath.img_produto)
    sleep(1)

    for x in range(3):
        interacao.clique(browser, xPath.next_right)
        sleep(0.5)

    for x in range(3):
        interacao.clique(browser, xPath.next_left)
        sleep(0.5)

    interacao.clique(browser, xPath.bt_fechar_imagem)
    sleep(1)
    interacao.clique(browser, xPath.bt_minha_localizacao)
    sleep(5)
    web.scrool_to(browser, 500)
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_email, pessoa.get('email'))
    sleep(1)
    interacao.preencherCampoEspecifico(browser, xPath.campo_telefone, gerar.ddd(pessoa) + gerar.telefone(pessoa), 11)
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_mensagem, 'teste')
    sleep(2)
    interacao.clique(browser, xPath.bt_solicitar_contato)
    sleep(2)
    web.scrool_to(browser, 2000)
    sleep(2)
    interacao.clique(browser, xPath.box_add_produto_relacionado_1)
    sleep(1)
    interacao.clique(browser, xPath.box_add_produto_relacionado_2)
    sleep(1)
    interacao.clique(browser, xPath.box_add_produto_relacionado_3)
    sleep(3)
    interacao.clique(browser, xPath.bt_comprar_junto)
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_nome_notificacao, pessoa.get('nome'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_email_notificacao, pessoa.get('email'))
    sleep(1)
    interacao.clique(browser, xPath.bt_cadastrar)