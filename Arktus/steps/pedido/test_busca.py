from time import sleep
import paths.xPath_pedido as xPath
import acoes.Interacoes as interacao
import acoes.generator as gerar
import navegador.browser as web

def buscar(browser, pessoa):
    interacao.preencherCampo(browser, xPath.campo_busca, 'teste')
    sleep(2)
    interacao.clique(browser, xPath.lupa_busca)