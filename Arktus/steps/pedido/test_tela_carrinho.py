from time import sleep
import paths.xPath_pedido as xPath
import acoes.Interacoes as interacao
import navegador.browser as web


def tela_carrinho(browser, pessoa):
    sleep(6)
    interacao.clique(browser, xPath.bt_menos_produto1)
    sleep(6)
    interacao.clique(browser, xPath.bt_mais_produto2)
    sleep(6)
    web.scrool_to(browser, 500)
    sleep(3)
    interacao.clique(browser, xPath.optional_receba_no_endereco)
    sleep(3)
    interacao.preencherCampoEspecifico(browser, xPath.campo_calcular_frete, pessoa.get('cep'), 8)
    sleep(2)
    interacao.clique(browser, xPath.lupa_busca_cep)
    sleep(5)
    interacao.clique(browser, xPath.bt_usar_localizacao)
    sleep(5)
    interacao.clique(browser, xPath.box_selecionar_transportadora)