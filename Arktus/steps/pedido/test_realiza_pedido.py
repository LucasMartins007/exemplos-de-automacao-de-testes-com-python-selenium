from time import sleep
import paths.xPath_pedido as xPath
import acoes.Interacoes as interacao
import steps.pedido.test_busca as busca
import steps.pedido.test_tela_produto as produto
import steps.pedido.test_tela_carrinho as carrinho
import steps.pedido.test_tela_checkout_identificacao as identificacao
import steps.pedido.formas_pagamento.test_deposito as deposito
import steps.pedido.formas_pagamento.test_boleto as boleto
import steps.pedido.formas_pagamento.test_cartao_credito as cartao


def realiza_pedido(browser, pessoa):
    print('\n       Buscando produto')
    busca.buscar(browser, pessoa)
    sleep(5)
    print('\n       Testando elementos da tela do produto')
    produto.tela_produto(browser, pessoa)
    sleep(5)
    interacao.clique(browser, xPath.icon_carrinho)
    sleep(2)

    if(interacao.elemento_existe(browser, xPath.bt_ver_carrinho)):
        interacao.clique(browser, xPath.bt_ver_carrinho)
    if(interacao.elemento_existe(browser, xPath.bt_ver_carrinho_2)):
        interacao.clique(browser, xPath.bt_ver_carrinho_2)
        
    sleep(5)
    print('\n       Testando elementos da tela do carrinho')
    carrinho.tela_carrinho(browser, pessoa)
    sleep(5)
    interacao.clique(browser, xPath.bt_fechar_pedido_carrinho)
    sleep(5)
    print('\n       Testando elementos da tela de identificação do checkout')
    identificacao.tela_checkout_identificacao(browser, pessoa)
    sleep(5)

def pagamento_via_deposito(browser, pessoa):
    print("\n   Pedido com pagamento via depósito..")
    realiza_pedido(browser, pessoa)
    sleep(2)
    deposito.pagamento_via_deposito(browser)
    sleep(15)
    interacao.clique(browser, xPath.home)


def pagamento_via_boleto(browser, pessoa):
    print("\n   Pedido com pagamento via boleto..")
    realiza_pedido(browser, pessoa)
    sleep(2)
    boleto.pagamento_via_boleto(browser)
    sleep(15)
    interacao.clique(browser, xPath.home)

def pagamento_via_cartao(browser, pessoa):
    print("\n   Pedido com pagamento via cartão..")
    realiza_pedido(browser, pessoa)
    sleep(2)
    cartao.pagamento_via_cartao(browser, pessoa)
    sleep(15)
    interacao.clique(browser, xPath.home)
