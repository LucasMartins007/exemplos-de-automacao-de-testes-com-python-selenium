import os
from time import sleep
from selenium import webdriver
import paths.xPath_cadastro as xPath
import erros.error_cadastro as erro
import acoes.Interacoes as interacao
import acoes.generator as gerar
import navegador.browser as web

def realizarCadastro(browser, pessoa):
    selectInformacaoTributaria = [xPath.selectContribuinte, xPath.selectNaoContribuinte, xPath.selectIsento]
    arquivo = '/documentos/doc_test_sintegra.pdf'
    interacao.clique(browser, xPath.cadastre_se)
    sleep(3)
    interacao.preencherCampo(browser, xPath.campoEmail, pessoa.get('email'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campoSenha, pessoa.get('senha'))
    sleep(1)
    interacao.clique(browser, xPath.mostrarSenha)
    sleep(1)
    interacao.clique(browser, xPath.mostrarSenha)
    sleep(1)
    interacao.preencherCampo(browser, xPath.campoDDD, gerar.ddd(pessoa))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campoTelefone, gerar.telefone(pessoa))    
    sleep(1)
    interacao.clique(browser, xPath.btCaixaCookie)
    sleep(1)
    interacao.selecionarOpcao(browser, xPath.campoInformacaoTributaria, selectInformacaoTributaria, 3)
    sleep(1)
    interacao.preencherCampoEspecifico(browser, xPath.campoCNPJ, gerar.cnpj(), 14)
    sleep(1)
    interacao.preencherCampo(browser, xPath.campoResponsavel, pessoa.get('nome') + ' responsável')
    sleep(1)
    interacao.preencherCampo(browser, xPath.campoRazaoSocial, pessoa.get('nome') + ' ltda.')
    sleep(1)
    interacao.preencherCampo(browser, xPath.campoInscricaoEstadual, gerar.ie())
    sleep(1)
    interacao.enviarArquivo(browser, xPath.file_comprovante_residencia, arquivo)
    sleep(1)
    interacao.enviarArquivo(browser, xPath.file_contrato_social, arquivo)
    sleep(1)
    interacao.enviarArquivo(browser, xPath.file_documentos_pessoais, arquivo)
    sleep(1)

    interacao.clique(browser, xPath.btCadastrar)
    
    if(erro.verificaErros(browser, pessoa) == False):
         print("Houve um erro no login, enviamos um email para o adm, e estamos reiniciando a rotina")
         sleep(2)    
         web.close(browser)
         web.restart()