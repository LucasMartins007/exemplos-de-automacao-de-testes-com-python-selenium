from time import sleep
import paths.xPath_altera_dados as xPath
import acoes.Interacoes as interacao


tipos_endereco = [xPath.select_adicional, xPath.select_proprietario, xPath.select_profissional, xPath.select_principal]

def exluir_endereco(browser):
    interacao.clique(browser, xPath.trash_excluir_endereco)

def teste_usar_minha_localizacao(browser, pessoa):
    interacao.clique(browser, xPath.usar_minha_localizacao)
    sleep(2)
    interacao.preencherCampo(browser, xPath.campo_numero, pessoa.get('numero'))
    sleep(1)
    interacao.selecionarOpcao(browser, xPath.select_endereco_principal, tipos_endereco, 4)
    sleep(2)
    interacao.clique(browser, xPath.bt_salvar_endereco)
    

def teste_endereco_principal(browser, pessoa):    
    interacao.clique(browser, xPath.local_adicionar_endereco)
    sleep(1)
    interacao.preencherCampoEspecifico(browser, xPath.campo_cep, pessoa.get('cep'), 8)
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_bairro, pessoa.get('bairro'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_rua, pessoa.get('endereco'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_complemento, pessoa.get('bairro'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_numero, pessoa.get('numero'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_cidade, pessoa.get('cidade'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_uf, pessoa.get('estado'))
    sleep(1)
    interacao.selecionarOpcao(browser, xPath.select_endereco_principal, tipos_endereco, 4)
    sleep(1)
    interacao.clique(browser, xPath.bt_salvar_endereco)
    sleep(2)
    interacao.clique(browser, xPath.pen_alterar_endereco)
    sleep(1)
    teste_usar_minha_localizacao(browser, pessoa)
    sleep(2)
    exluir_endereco(browser)
    
    if(interacao.elemento_existe(browser, xPath.bt_confirmar_excluir_endereco)):
        sleep(2)
        interacao.clique(browser, xPath.bt_confirmar_excluir_endereco)
        
    sleep(2)
    