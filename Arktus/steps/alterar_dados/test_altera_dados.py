from time import sleep
import paths.xPath_altera_dados as xPath
import acoes.Interacoes as interacao
import steps.alterar_dados.test_altera_endereco as altera_endreco
import steps.alterar_dados.test_altera_dados_pessoais as altera_dados_pessoais
import steps.alterar_dados.test_altera_email as altera_email
import steps.alterar_dados.test_altera_telefone as altera_telefone
import steps.alterar_dados.test_altera_senha as altera_senha
import acoes.generator as gerar
import navegador.browser as web

import erros.error_altera_dados as error

def acessar_meus_dados(browser):
    interacao.clique(browser, xPath.avatar)
    sleep(2)
    interacao.clique(browser, xPath.minha_conta)
    sleep(5)
    interacao.clique(browser, xPath.meus_dados)
    sleep(2)

def alteraDados(browser):
    acessar_meus_dados(browser)
    pessoa = gerar.pessoa()

# Altera dados pessoais
    print("     Alteração de todos os campos dos dados pessoais..")
    altera_dados_pessoais.teste_dados_pessoais(browser, pessoa)
    pessoa_atualizada = gerar.pessoa()

# Alteração de endereco
    print("     Alterando endereço..")
    interacao.clique(browser, xPath.lista_enderecos)
    sleep(1)
    web.refresh(browser)
    sleep(2)
    altera_endreco.teste_endereco_principal(browser, pessoa_atualizada)
    sleep(1)

# Altera telefone
    print("     Alterando telefones..")
    interacao.clique(browser, xPath.lista_telefones)
    sleep(1)
    altera_telefone.teste_altera_telefone(browser, pessoa_atualizada)
    sleep(1)

# Altera Email
    print("     Alterando emails..")
    interacao.clique(browser, xPath.lista_email)
    sleep(2)
    altera_email.alterar_email(browser, pessoa_atualizada)
    sleep(1)

# Altera senha
    altera_senha.teste_atualizar_senha(browser, pessoa_atualizada)
    sleep(1)
    web.close(browser)
    
