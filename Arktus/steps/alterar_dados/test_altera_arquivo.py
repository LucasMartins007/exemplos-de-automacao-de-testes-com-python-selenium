from time import sleep
import paths.xPath_altera_dados as xPath
import acoes.Interacoes as interacao
import navegador.browser as web

def teste_altera_arquivo(browser, pessoa):
    arquivo = '/documentos/doc_test_sintegra.pdf'

    web.scrool_bottom(browser)
    sleep(1)
    interacao.enviarArquivo(browser, xPath.file_comprovante_residencia, arquivo)
    sleep(1)
    interacao.clique(browser, xPath.bt_enviar_arquivo_receita_pj)
    sleep(3)
    interacao.clique(browser, xPath.pen_alterar_dados_pessoais_arquivos)
    sleep(1)

    sleep(1)
    interacao.enviarArquivo(browser, xPath.file_contrato_social, arquivo)
    sleep(1)
    interacao.clique(browser, xPath.bt_enviar_arquivo_contrato_social)
    sleep(3)
    interacao.clique(browser, xPath.pen_alterar_dados_pessoais_arquivos)
    sleep(1)

    sleep(1)
    interacao.enviarArquivo(browser, xPath.file_documentos_pessoais, arquivo)
    sleep(1)
    interacao.clique(browser, xPath.bt_enviar_arquivo_documentos_pessoais)
    sleep(3)
    interacao.clique(browser, xPath.pen_alterar_dados_pessoais_arquivos)
    sleep(1)
    browser.execute_script("window.scrollTo(0, 0);")