from steps.alterar_dados.test_altera_arquivo import teste_altera_arquivo
from steps.alterar_dados.test_altera_endereco import teste_endereco_principal
from steps.alterar_dados.test_altera_telefone import teste_altera_telefone
from time import sleep
import paths.xPath_altera_dados as xPath
import acoes.Interacoes as interacao

def teste_dados_pessoais(browser, pessoa):
    interacao.clique(browser, xPath.dados_pessoais)
    sleep(2)
    interacao.clique(browser, xPath.pen_alterar_dados_pessoais)
    sleep(2)
    interacao.preencherCampo(browser, xPath.campo_meus_dados_email, pessoa.get('email'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_meus_dados_responsavel, pessoa.get('nome'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_meus_dados_razao_social, pessoa.get('nome') + ' ltda.')
    sleep(1)
    interacao.clique(browser, xPath.bt_salvar_meus_dados)

# TESTE DAS 'PEN' DENTRO DO MENU DE DADOS PESSOAIS

# Telefone
    sleep(1)
    interacao.clique(browser, xPath.pen_alterar_dados_pessoais_telefones)
    teste_altera_telefone(browser, pessoa)

    sleep(1)
    interacao.clique(browser, xPath.dados_pessoais)
    
# Endereço
    sleep(3)
    interacao.clique(browser, xPath.pen_alterar_dados_pessoais_endereco)
    sleep(1)
    teste_endereco_principal(browser, pessoa)

    sleep(1)
    interacao.clique(browser, xPath.dados_pessoais)

# Arquivos
    sleep(3)
    interacao.clique(browser, xPath.pen_alterar_dados_pessoais_arquivos)
    teste_altera_arquivo(browser, pessoa)
    
    sleep(1)