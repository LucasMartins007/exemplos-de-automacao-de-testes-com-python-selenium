from time import sleep
import paths.xPath_altera_dados as xPath
import acoes.Interacoes as interacao
import acoes.generator as gerar

def teste_atualizar_senha(browser, pessoa):
    interacao.clique(browser, xPath.alterar_senha)
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_senha_atual, pessoa.get('senha'))
    sleep(1)

    
    newPessoa = gerar.pessoa()
    interacao.preencherCampo(browser, xPath.campo_nova_senha, newPessoa.get('senha'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_confirma_nova_senha, newPessoa.get('senha'))
    sleep(1)
    interacao.clique(browser, xPath.bt_salvar_senha)
    sleep(2)
