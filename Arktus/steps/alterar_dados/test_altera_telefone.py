from time import sleep
import paths.xPath_altera_dados as xPath
import acoes.Interacoes as interacao
import acoes.generator as gerar

def teste_altera_telefone(browser, pessoa):
    tipos_telefone =[xPath.select_telefone_principal, xPath.select_celular, xPath.select_residencial, xPath.select_comercial, xPath.select_fax, xPath.select_adicional]
    
    interacao.clique(browser, xPath.tel_adicionar_telefone)
    sleep(1)
    interacao.preencherCampoEspecifico(browser, xPath.campo_ddd, gerar.ddd(pessoa), 2)
    sleep(1)
    interacao.preencherCampoEspecifico(browser, xPath.campo_telefone, gerar.telefone(pessoa), 9)
    sleep(1)
    interacao.selecionarOpcao(browser, xPath.select_tipo_telefone, tipos_telefone, 6)
    sleep(1)
    interacao.clique(browser, xPath.bt_salvar_telefone)
    sleep(1)
    interacao.clique(browser, xPath.trash_excluir_telefone)
    sleep(1)
    
    if(interacao.elemento_existe(browser, xPath.bt_confirmar_excluir_telefone)):
        sleep(2)
        interacao.clique(browser, xPath.bt_confirmar_excluir_telefone)