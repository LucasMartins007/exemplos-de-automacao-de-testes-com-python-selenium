from time import sleep
import paths.xPath_altera_dados as xPath
import acoes.Interacoes as interacao


def alterar_email(browser, pessoa):
    interacao.clique(browser, xPath.plus_adicionar_email)
    sleep(2)
    interacao.preencherCampo(browser, xPath.campo_nome, pessoa.get('nome'))
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_email, pessoa.get('email'))
    sleep(1)
    interacao.clique(browser, xPath.box_preferencial)
    sleep(1)
    interacao.clique(browser, xPath.box_preferencial)
    sleep(1)
    interacao.clique(browser, xPath.box_preferencial)
    sleep(1)
    interacao.clique(browser, xPath.bt_salvar_email)
    sleep(1)
    interacao.clique(browser, xPath.trash_excluir_email)
    if(interacao.elemento_existe(browser, xPath.bt_confirmar_excluir_email)):
        sleep(2)
        interacao.clique(browser, xPath.bt_confirmar_excluir_email)
    
    sleep(2)