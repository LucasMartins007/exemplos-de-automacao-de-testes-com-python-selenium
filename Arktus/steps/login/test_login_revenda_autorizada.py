from time import sleep
import paths.xPath_login as xPath
import erros.error_login as error
import acoes.Interacoes as interacao
import navegador.browser as web

def realizarLogin(browser, pessoa):
    sleep(4)
    interacao.clique(browser, xPath.link_entre)
    sleep(4)
    interacao.preencherCampo(browser, xPath.campo_login, 'testelucas@teste.com')
    sleep(1)
    interacao.preencherCampo(browser, xPath.campo_senha, '123456')
    sleep(1)
    interacao.clique(browser, xPath.bt_logar)
    sleep(1)

    if(error.verificaErros(browser, pessoa) == False):
        print("Houve um erro no login, enviamos um email para o adm, e estamos reiniciando a rotina")
        sleep(2)
        web.close(browser)
        web.restart()