from time import sleep
import paths.xPath_pedido as xPath
import acoes.Interacoes as interacao
import acoes.generator as gerar
import navegador.browser as web

def valida_valor_para_calculo(value):
    value = value.replace("R", "")
    value = value.replace("$", "")
    value = value.replace(" ", "")
    value = value.replace(".", "")
    return value

def valida_valores_carrinho(qtde_produto, vl_unitario, vl_total_produto, vl_total_carrinho):
    vl_unitario = valida_valor_para_calculo(vl_unitario)
    vl_total_produto = valida_valor_para_calculo(vl_total_produto)
    vl_total_carrinho = valida_valor_para_calculo(vl_total_carrinho)

    total_produto_real = qtde_produto * vl_unitario
    if(vl_total_produto != total_produto_real):
        print("Calculo valor produto errado")
        return
    if(vl_total_carrinho != total_produto_real):
        print("Calculo valor carrinho errado")
        return
    print("Valores do carrinho estão corretos")