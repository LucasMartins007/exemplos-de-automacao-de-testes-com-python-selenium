import smtplib
import mimetypes
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage


def envia_msg_erro(campo_erro, browser, message):
    filename = 'screenshots/erro_'+ campo_erro +'.png'
    browser.save_screenshot(filename)
    message = message + campo_erro
    enviaEmail(message, filename)

def adiciona_anexo(msg, filename):
        if not os.path.isfile(filename):
                return

        ctype, encoding = mimetypes.guess_type(filename)

        if ctype is None or encoding is not None:
                ctype = 'application/octet-stream'
        
        with open(filename, 'rb') as f:
                mime = MIMEImage(f.read(), _subtype='png')

        mime.add_header('Content-Disposition', 'attachment', filename=filename)
        msg.attach(mime)

def enviaEmail(message, filename):
    
        host = 'email-1.smartbr.com'
        port = 587
        remetente = 'lucasmartins@smartbr.com'
        destinatario = 'lucasrodriguesmartinsoliveira@gmail.com'
        password = 'LRM105'
        server = smtplib.SMTP(host, port)
        server.ehlo()
        server.starttls()
        server.login(remetente, password)
                        
        msg = MIMEMultipart()
        msg['From'] = remetente
        msg['to'] = destinatario
        msg['Subject'] = 'Exceção encontrada nos casos de teste!'

        msg.attach(MIMEText(message, 'html', 'utf-8'))

        adiciona_anexo(msg, filename)

        raw = msg.as_string()

        server.sendmail(remetente, destinatario, raw)
        print('Mensagem enviada!')
        server.quit()