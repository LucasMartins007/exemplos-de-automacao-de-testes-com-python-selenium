from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
import random
from time import sleep
import os
import navegador.browser as web

def clique(browser, path):
    if(elemento_existe(browser, path)):
        elemento = browser.find_element_by_xpath(path)
        elemento.click()
    else:
        print("Botão não foi encontrado -> " + path)
        sleep(3600)
        web.close(browser)


def preencherCampo(browser, path, value):
    elemento = browser.find_element_by_xpath(path)
    if(elemento_existe(browser, path)):
        if(elemento.is_enabled()):
            for x in range(50):
                elemento.send_keys(Keys.BACKSPACE)
            sleep(0.5)
            elemento.send_keys(value)
    else:
        print("Campo não foi encontrado -> " + path)
        sleep(3600)
        web.close(browser)
        

def preencherCampoEspecifico(browser, path, value, size):
    elemento = browser.find_element_by_xpath(path)
    sleep(0.5)
    elemento.clear()
    sleep(0.5)
    for x in range(size+1):
        elemento.send_keys(Keys.BACKSPACE)
    for x in range(size):
        elemento.send_keys(value[x])

def selecionarOpcao(browser, pathSelect, paths, qtde):
    clique(browser, pathSelect)
    sleep(0.5)
    x = random.randint(0, qtde-1)
    sleep(0.5)
    clique(browser, paths[x])

def enviarArquivo(browser, path, value):
    elemento = browser.find_element_by_xpath(path)
    sleep(0.5)
    elemento.clear()
    sleep(0.5)
    elemento.send_keys(os.getcwd() + value)

def get_value_from_input(browser, path):
    elemento = browser.find_element_by_xpath(path)
    value = elemento.get_attribute('value')
    return value

def get_value_from_span(browser, path):
    elemento = browser.find_element_by_xpath(path)
    value = elemento.get_attribute('innerText')
    return value
        
def elemento_existe(browser, path):
    if((len(browser.find_elements_by_xpath(path)) > 0)):
        return True