from fordev.generators import people, uf
from fordev.generators import cnpj as generateCNPJ, state_registration as generateIE

def pessoa():
    estado = uf()
    pessoa = people(uf_code=estado[0], sex='F', formatting=False)
    return pessoa

def ddd(pessoa):
    telefone = pessoa.get('celular').split()
    telefone = telefone[0]
    return telefone[0:2]

def telefone(pessoa):
    telefone = pessoa.get('celular').split()
    telefone = telefone[0]
    return telefone[1:12]

def cnpj():
    cnpj = generateCNPJ(formatting=False)
    return cnpj

def ie():
    ie = generateIE(uf_code = 'SP', formatting = True, data_only = True)
    return ie