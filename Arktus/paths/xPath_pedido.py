home = '/html/body/div[1]/header/div/div/div[1]/a'
campo_busca = '/html/body/div[1]/header/div[2]/div/div[2]/form/div/div/input'
'/html/body/div[5]/div[3]/ul/li[3]/div[4]/div[2]/a'
lupa_busca = '/html/body/div[1]/header/div[2]/div/div[2]/form/div/div/div/button'

card_produto = '/html/body/div[1]/main/section/div[3]/div/div[2]/div/div[2]/div/div[1]/div[1]/a/img'

icon_carrinho = '/html/body/div[1]/header/div[2]/div/div[3]/button[2]'

campo_cep = '/html/body/div[1]/main/div/div[1]/div[1]/div/div[3]/ul/li[8]/div/div[1]/div/div/div/input'
campo_email = '/html/body/div[1]/main/div/div[1]/div[2]/div/div[2]/form/div/div[4]/div[1]/div/div[1]/div/div/input'
campo_telefone = '/html/body/div[1]/main/div/div[1]/div[2]/div/div[2]/form/div/div[4]/div[1]/div/div[2]/div/div/input'
campo_mensagem = '/html/body/div[1]/main/div/div[1]/div[2]/div/div[2]/form/div/div[4]/div[2]/div/div/textarea'

img_produto = '/html/body/div[1]/main/div/div[1]/div[1]/div/div[2]/div/div[3]/div/div/div/div[1]'
next_right = '/html/body/div[1]/main/div/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/button[2]'
next_left  = '/html/body/div[1]/main/div/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/button[1]'

box_add_produto_relacionado_1 = '/html/body/div[1]/main/div/section[1]/div/div/div[3]/ul/li[1]/div[1]/span/span[1]/input'
box_add_produto_relacionado_2 = '/html/body/div[1]/main/div[2]/section[1]/div/div/div[3]/ul/li[2]/div[1]/span/span[1]/input'
box_add_produto_relacionado_3 = '/html/body/div[1]/main/div[2]/section[1]/div/div/div[3]/ul/li[3]/div[1]/span/span[1]/input'

seta_esquerda = '/html/body/div[1]/main/div[2]/div[1]/div[1]/div/div[2]/div/div[3]/div/button[2]'
seta_direita = '/html/body/div[1]/main/div[2]/div[1]/div[1]/div/div[2]/div/div[3]/div/button[1]'
bt_fechar_imagem = '/html/body/div[1]/main/div/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/button[1]'

bt_add_carrinho = '/html/body/div[1]/main/div/div[1]/div[1]/div/div[3]/ul/li[4]/div/button'
bt_busca_cep = '/html/body/div[1]/main/div/div[1]/div[1]/div/div[3]/ul/li[8]/div/div[1]/div/div/div/div/button'
bt_minha_localizacao  = '/html/body/div[1]/main/div/div[1]/div[1]/div/div[3]/ul/li[8]/div/div[2]/button'
bt_solicitar_contato = '/html/body/div[1]/main/div/div[1]/div[2]/div/div[2]/form/div/div[4]/div[3]/button'
bt_comprar_junto = '/html/body/div[1]/main/div[2]/section[1]/div/div/div[4]/div/div[2]/button'

#card_carrinho
bt_ver_carrinho = '/html/body/div[5]/div[3]/ul/li[3]/div[4]/div[2]/a'
bt_ver_carrinho_2 = '/html/body/div[5]/div[3]/ul/li[3]/div[5]/div[2]/a'
bt_fechar_pedido = '/html/body/div[5]/div[3]/ul/li[3]/div[4]/div[1]/a'
campo_frete = '/html/body/div[5]/div[3]/ul/li[3]/div[1]/div/div/div/input'

#CARRINHO DE COMPRAS

# Card Produto
trash_excluir_produto1_carrinho = '/html/body/div[1]/main/section/div/div/div[1]/div/div[1]/div[2]/button'
trash_excluir_produto2_carrinho = '/html/body/div[1]/main/section/div/div/div[1]/div/div[2]/div[2]/button'
trash_excluir_produto3_carrinho = '/html/body/div[1]/main/section/div/div/div[1]/div/div[3]/div[2]/button'
bt_menos_produto1 = '/html/body/div[1]/main/section/div/div/div[1]/div/div[1]/div[3]/div[1]/div/div[2]/div/button[1]'
bt_menos_produto2 = '/html/body/div[1]/main/section/div/div/div[1]/div/div[2]/div[3]/div[1]/div/div[2]/div/button[1]'
bt_menos_produto3 = '/html/body/div[1]/main/section/div/div/div[1]/div/div[3]/div[3]/div[1]/div/div[2]/div/button[1]'
bt_menos_produto4 = '/html/body/div[1]/main/section/div/div/div[1]/div/div[4]/div[3]/div[1]/div/div[2]/div/button[1]'

bt_mais_produto1 = '/html/body/div[1]/main/section/div/div/div[1]/div/div[1]/div[3]/div[1]/div/div[2]/div/button[2]'
bt_mais_produto2 = '/html/body/div[1]/main/section/div/div/div[1]/div/div[2]/div[3]/div[1]/div/div[2]/div/button[2]'
bt_mais_produto3 = '/html/body/div[1]/main/section/div/div/div[1]/div/div[3]/div[3]/div[1]/div/div[2]/div/button[2]'
bt_mais_produto4 = '/html/body/div[1]/main/section/div/div/div[1]/div/div[4]/div[3]/div[1]/div/div[2]/div/button[2]'

bt_fechar_cookie = '/html/body/div[1]/main/div/div/div/div/div/div[2]/div/button[2]'

qtde_produto = '/html/body/div[1]/main/section/div/div/div[1]/div/div[1]/div[3]/div[1]/div/div[2]/div/input'
vl_unitario = '/html/body/div[1]/main/section/div/div/div[1]/div/div[1]/div[3]/div[2]/p[2]'
vl_total_produto = '/html/body/div[1]/main/section/div/div/div[1]/div/div[1]/div[3]/div[2]/p[3]'
vl_total_carrinho = '/html/body/div[1]/main/section/div/div/div[2]/div/div[1]/div[1]/div[1]/div'



# Card receba no endereço
optional_receba_no_endereco = '/html/body/div[1]/main/section/div/div/div[1]/div/div[5]/div[1]/div/div/div/label/span[1]/span[1]/input'
campo_calcular_frete = '/html/body/div[1]/main/section/div/div/div[1]/div/div[5]/div[2]/div/div/div[1]/div/div/input'
box_selecionar_transportadora = '/html/body/div[1]/main/section/div/div/div[1]/div/div[5]/div[2]/div/ul/li/div/label/span[1]/span[1]/input'
lupa_busca_cep = '/html/body/div[1]/main/section/div/div/div[1]/div/div[5]/div[2]/div/div/div[1]/div/div/div/button'
bt_usar_localizacao = '/html/body/div[1]/main/section/div/div/div[1]/div/div[5]/div[2]/div/div/div[2]/button'


# Resumo do carrinho
bt_fechar_pedido_carrinho = '/html/body/div[1]/main/section/div/div/div[2]/div/div[2]/a'
bt_continuar_comprando = '/html/body/div[1]/main/section/div/div/div[2]/div/div[3]/a'


# Footer notificação
campo_nome_notificacao = '/html/body/div[1]/footer/div[1]/div/div[2]/form/div/div[1]/div/div/input'
campo_email_notificacao= '/html/body/div[1]/footer/div[1]/div/div[2]/form/div/div[2]/div/div/input'
bt_cadastrar = '/html/body/div[1]/footer/div[1]/div/div[2]/form/div/div[3]/button'


# Checkout - Identificação
bt_novo_endereco = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div[1]/div[1]/button'
bt_usar_localizacao_identificacao = '/html/body/div[5]/div[3]/div/div[1]/div/div/form/div/div[2]/button'

campo_cep_identificacao = '/html/body/div[5]/div[3]/div/div[1]/div/div/form/div/div[1]/div/input'
campo_bairro = '/html/body/div[5]/div[3]/div/div[1]/div/div/form/div/div[3]/div/div/div/div[1]/div/input'
campo_rua = '/html/body/div[5]/div[3]/div/div[1]/div/div/form/div/div[3]/div/div/div/div[2]/div/input'
campo_complemento = '/html/body/div[5]/div[3]/div/div[1]/div/div/form/div/div[3]/div/div/div/div[3]/div[1]/div/div/input'
campo_numero = '/html/body/div[5]/div[3]/div/div[1]/div/div/form/div/div[3]/div/div/div/div[3]/div[2]/div/div/input'
campo_cidade = '/html/body/div[5]/div[3]/div/div[1]/div/div/form/div/div[3]/div/div/div/div[4]/div[1]/div/div/input'
campo_uf = '/html/body/div[5]/div[3]/div/div[1]/div/div/form/div/div[3]/div/div/div/div[4]/div[2]/div/div/input'
select_tipo_endereco = '/html/body/div[5]/div[3]/div/div[1]/div/div/form/div/div[3]/div/div/div/div[5]/div/div'
select_principal = '/html/body/div[6]/div[3]/ul/li[1]'
select_proprietario = '/html/body/div[6]/div[3]/ul/li[2]'
select_profissional = '/html/body/div[6]/div[3]/ul/li[3]'
select_adicional = '/html/body/div[6]/div[3]/ul/li[4]'

box_endereco1 = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div[1]/div[2]/div/div/div/div/label[1]/span[1]/span[1]/input'
box_endereco2 = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div[1]/div[2]/div/div/div/div/label[2]/span[1]/span[1]/input'

box_transportadora1 = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[1]/div/label/span[1]/span[1]/input'
box_transportadora2 = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[2]/div/label/span[1]/span[1]/input'

bt_salvar_endereco = '/html/body/div[5]/div[3]/div/div[1]/div/div/form/div/div[3]/div/div/div/div[6]/button'
bt_cancelar = '/html/body/div[5]/div[3]/div/div[2]/button'


# Checkout - Entrega

pen_endereco = '/html/body/div[1]/main/section/div/div/div/div[2]/div[1]/div[3]/div[1]/div[2]/button'
pen_forma_envio = '/html/body/div[1]/main/section/div/div/div/div[2]/div[1]/div[3]/div[2]/div[2]/button'

# CARD DEPOSITO
abrir_card_deposito = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[1]/div/div[1]'
bt_finalizar_pagamento_deposito = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[1]/div/div[2]/div/div/div/div/div/div[2]/div/button'
bt_cancelar = '/html/body/div[5]/div[3]/div/div[2]/button'


# CARD CT
abrir_card_ct = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[2]/div/div[1]'
bt_adicionar_cartao = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[2]/div/div[2]/div/div/div/div/div/div[2]/button'
bt_remover_cartao = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[2]/div/div[2]/div/div/div/div/div/div[2]/div[1]/div[2]/div[2]/button'
campo_numero_cartao = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[2]/div/div[2]/div/div/div/div/div/form/div/div[2]/div/div/div/input'
campo_nome_titular = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[2]/div/div[2]/div/div/div/div/div/form/div/div[3]/div/div/div/input'
campo_data_validade = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[2]/div/div[2]/div/div/div/div/div/form/div/div[4]/div/div[1]/div/div/div/input'
campo_cod_seguranca  = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[2]/div/div[2]/div/div/div/div/div/form/div/div[4]/div/div[2]/div/div/div/input'
select_parcelamento = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[2]/div/div[2]/div/div/div/div/div/form/div/div[5]/div/div/div/div/div'
select_parcelamento_1x = '/html/body/div[5]/div[3]/ul/li[1]'
select_parcelamento_2x = '/html/body/div[8]/div[3]/ul/li[2]'
select_parcelamento_3x = '/html/body/div[5]/div[3]/ul/li[3]'
bt_finalizar_pagamento_cartao = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[2]/div/div[2]/div/div/div/div/div/div[4]/div/button'


# CARD FINANCEIRA
abrir_card_financeira = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[3]/div/div[1]'
select_carencia_30d = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[3]/div/div[2]/div/div/div/div/div/div[2]/div[1]/div/div/div/div'
select_carencia_30d_30x = '/html/body/div[5]/div[3]/ul/li[7]'

select_carencia_60d = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[3]/div/div[2]/div/div/div/div/div/div[2]/div[2]/div/div/div/div'
select_carencia_60d_30x = '/html/body/div[6]/div[3]/ul/li[7]'

select_carencia_90d = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[3]/div/div[2]/div/div/div/div/div/div[2]/div[3]/div/div/div/div'
select_carencia_90d_30x = '/html/body/div[7]/div[3]/ul/li[7]'

bt_finalizar_pagamento_financeira = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[3]/div/div[2]/div/div/div/div/div/div[3]/div/button'


# CARD BOLETO
abrir_card_boleto = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[4]/div/div[1]'
bt_finalizar_pagamento_boleto = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[4]/div/div[2]/div/div/div/div/div/div[2]/div/button'
bt_imprimir_boleto = '/html/body/div[1]/main/section/div/div[2]/div[2]/div/div/div[2]/button'

# CARD PIX
abrir_card_pix = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[5]/div/div[1]'
bt_finalizar_pagamento_pix = '/html/body/div[1]/main/section/div/div/div/div[2]/div[2]/div/div/div/ul/li[5]/div/div[2]/div/div/div/div/div/div[2]/div/button'