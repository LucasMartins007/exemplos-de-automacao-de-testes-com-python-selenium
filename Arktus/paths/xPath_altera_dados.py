
avatar = '/html/body/div[1]/header/div[2]/div/div[3]/button[1]'
minha_conta = '/html/body/div[8]/div[3]/ul/li[1]/a'
meus_dados = '/html/body/div[1]/main/section/div[2]/div[1]/nav/div/div[3]/div[1]/div[1]'

# DADOS PESSOAIS
dados_pessoais = '/html/body/div[1]/main/section/div[2]/div[1]/nav/div/div[3]/div[2]/div/div/div/div/ul/li[1]'

pen_alterar_dados_pessoais = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[1]/div/h2/span/button'
pen_alterar_dados_pessoais_telefones = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[2]/div/h2/button'
pen_alterar_dados_pessoais_endereco = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[3]/div/h2/button'
pen_alterar_dados_pessoais_arquivos = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[4]/div/h2/span/button'

campo_meus_dados_email = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[1]/div/div/div/form/div[1]/div[1]/div/input'
campo_meus_dados_responsavel = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[1]/div/div/div/form/div[1]/div[2]/div[1]/div/div/input'
campo_meus_dados_razao_social  = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[1]/div/div/div/form/div[1]/div[2]/div[2]/div/div/input'

box_receber_email = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[1]/div/div/div/form/div[1]/div[4]/label[1]/span[1]/span[1]/input'
box_receber_ligacao = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[1]/div/div/div/form/div[1]/div[4]/label[2]/span[1]/span[1]/input'
box_receber_sms = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[1]/div/div/div/form/div[1]/div[4]/label[3]/span[1]/span[1]/input'

bt_salvar_meus_dados = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[1]/div/div/div/form/div[2]/button'
bt_cancelar_meus_dados = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[1]/div/div/div/button'

bt_enviar_arquivo_receita_pj = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[4]/div/div[1]/form[1]/div/div[2]/button'
bt_enviar_arquivo_contrato_social = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[4]/div/div[1]/form[2]/div/div[2]/button'
bt_enviar_arquivo_documentos_pessoais = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[4]/div/div[1]/form[3]/div/div[2]/button'

# LISTA DE TELEFONES 
lista_telefones = '/html/body/div[1]/main/section/div[2]/div[1]/nav/div/div[3]/div[2]/div/div/div/div/ul/li[2]'

tel_adicionar_telefone = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/h2/span/button'
pen_alterar_telefone = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/div[2]/button[1]'
trash_excluir_telefone = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/div[2]/button[2]'

campo_ddd = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/form/div[1]/div[1]/div/div/input'
campo_telefone = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/form/div[1]/div[2]/div/div/input'

select_tipo_telefone = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/form/div[1]/div[3]/div/div/div'
select_telefone_principal = '/html/body/div[11]/div[3]/ul/li[1]'
select_celular = '/html/body/div[11]/div[3]/ul/li[2]'
select_residencial = '/html/body/div[11]/div[3]/ul/li[3]'
select_comercial = '/html/body/div[11]/div[3]/ul/li[4]'
select_fax = '/html/body/div[11]/div[3]/ul/li[5]'
select_adicional = '/html/body/div[11]/div[3]/ul/li[6]'

bt_salvar_telefone = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/form/div[2]/button[1]'
bt_cancelar_telefone = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/form/div[2]/button[2]'
bt_confirmar_excluir_telefone = '/html/body/div[11]/div[3]/div/div[3]/button[1]'



# LISTA DE ENDEREÇOS
lista_enderecos = '/html/body/div[1]/main/section/div[2]/div[1]/nav/div/div[3]/div[2]/div/div/div/div/ul/li[3]'

local_adicionar_endereco = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/h2/span/button'
usar_minha_localizacao = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/form/div/div[2]/button'

pen_alterar_endereco = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div[2]/button[1]'
trash_excluir_endereco = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div[2]/button[2]'

campo_cep = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/form/div/div[1]/div/input'
campo_bairro = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/form/div/div[3]/div/div/div/div[1]/div/input'
campo_rua = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/form/div/div[3]/div/div/div/div[2]/div/input'
campo_complemento = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/form/div/div[3]/div/div/div/div[3]/div[1]/div/div/input'
campo_numero = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/form/div/div[3]/div/div/div/div[3]/div[2]/div/div/input'
campo_cidade = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/form/div/div[3]/div/div/div/div[4]/div[1]/div/div/input'
campo_uf = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/form/div/div[3]/div/div/div/div[4]/div[2]/div/div/input'

select_endereco_principal = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/form/div/div[3]/div/div/div/div[5]/div/div'
select_principal = '/html/body/div[11]/div[3]/ul/li[1]'
select_proprietario = '/html/body/div[11]/div[3]/ul/li[2]'
select_profissional = '/html/body/div[11]/div[3]/ul/li[3]'
select_adicional = '/html/body/div[11]/div[3]/ul/li[4]'

bt_salvar_endereco = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/form/div/div[3]/div/div/div/div[6]/button'
bt_cancelar_endereco = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div/button'
bt_confirmar_excluir_endereco = '/html/body/div[11]/div[3]/div/div[3]/button[1]'



# LISTA DE EMAILS
lista_email = '/html/body/div[1]/main/section/div[2]/div[1]/nav/div/div[3]/div[2]/div/div/div/div/ul/li[4]'

plus_adicionar_email = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/h2/span/button'
pen_alterar_email = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/div[2]/button[1]'
trash_excluir_email = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[2]/div[2]/button[2]'

campo_nome = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/form/div[1]/div[1]/div/div/input'
campo_email = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/form/div[1]/div[2]/div/div/input'

box_preferencial = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/form/div[1]/div[3]/div/label/span[1]/span[1]/input'

bt_salvar_email = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/form/div[2]/button[1]'
bt_cancelar_email = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/ul/div[1]/form/div[2]/button[2]'
bt_confirmar_excluir_email = '/html/body/div[11]/div[3]/div/div[3]/button[1]' 



# ALTERAR SENHA
alterar_senha = '/html/body/div[1]/main/section/div[2]/div[1]/nav/div/div[3]/div[2]/div/div/div/div/ul/li[5]'

campo_senha_atual = '/html/body/div[1]/main/section/div[2]/div[2]/form/section/div/div[2]/div/div/input'
campo_nova_senha = '/html/body/div[1]/main/section/div[2]/div[2]/form/section/div/div[3]/div/div/input'
campo_confirma_nova_senha = '/html/body/div[1]/main/section/div[2]/div[2]/form/section/div/div[4]/div/div/input'

bt_salvar_senha = '/html/body/div[1]/main/section/div[2]/div[2]/form/section/div/div[5]/button'


file_comprovante_residencia = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[4]/div/div[1]/form[1]/div/div[1]/div/div/input'
file_contrato_social = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[4]/div/div[1]/form[2]/div/div[1]/div/div/input'
file_documentos_pessoais = '/html/body/div[1]/main/section/div[2]/div[2]/section/div/div[4]/div/div[1]/form[3]/div/div[1]/div/div/input'