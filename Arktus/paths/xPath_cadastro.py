
cadastre_se = '//*[@id="__next"]/header/div[2]/div/div[3]/div/div/a[2]'
campoEmail = '//*[@id="__next"]/main/section/div/div/div/div/div/form/div[2]/div[1]/div/input'
campoSenha = '//*[@id="__next"]/main/section/div/div/div/div/div/form/div[2]/div[2]/div/input'
mostrarSenha = '//*[@id="__next"]/main/section/div/div/div/div/div/form/div[2]/div[2]/div/button'
campoDDD = '//*[@id="__next"]/main/section/div/div/div/div/div/form/div[2]/div[3]/div[1]/div/div/input'
campoTelefone = '//*[@id="__next"]/main/section/div/div/div/div/div/form/div[2]/div[3]/div[2]/div/div/input'
campoInformacaoTributaria='//*[@id="mui-component-select-type.pj.indicatorIERecipient"]'

file_comprovante_residencia = '/html/body/div[1]/main/section/div/div/div/div/div/form/div[2]/div[7]/div/input'
file_contrato_social = '/html/body/div[1]/main/section/div/div/div/div/div/form/div[2]/div[8]/div/input'
file_documentos_pessoais = '/html/body/div[1]/main/section/div/div/div/div/div/form/div[2]/div[9]/div/input'

selectContribuinte='//*[@id="menu-type.pj.indicatorIERecipient"]/div[3]/ul/li[1]'
selectNaoContribuinte='//*[@id="menu-type.pj.indicatorIERecipient"]/div[3]/ul/li[2]'
selectIsento='//*[@id="menu-type.pj.indicatorIERecipient"]/div[3]/ul/li[3]'

campoResponsavel='//*[@id="__next"]/main/section/div/div/div/div/div/form/div[2]/div[5]/div[1]/div/div/input'
campoRazaoSocial='//*[@id="__next"]/main/section/div/div/div/div/div/form/div[2]/div[5]/div[2]/div/div/input'
campoCNPJ='//*[@id="__next"]/main/section/div/div/div/div/div/form/div[2]/div[6]/div[1]/div/div/input'

campoInscricaoEstadual='//*[@id="__next"]/main/section/div/div/div/div/div/form/div[2]/div[6]/div[2]/div/div/input'

btCadastrar = '//*[@id="__next"]/main/section/div/div/div/div/div/form/div[3]/button/span[1]'
btCaixaCookie = '//*[@id="__next"]/main/div/div/div/div/div/div[2]/div/button[2]'