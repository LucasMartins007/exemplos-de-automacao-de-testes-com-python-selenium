from email import message
from emailconfig.send_email import enviaEmail
from warnings import catch_warnings
import test_processos as processos
import acoes.generator as gerar
from time import sleep
import logging

def rotina():
    try:
        pessoa = gerar.pessoa()
        
        processos.processo_cadastro(pessoa)
        processos.processo_login(pessoa)
        processos.processo_alterar_dados(pessoa)
        processos.processo_realizar_pedido(pessoa)
        
        sleep(2)
    except Exception as e:
        logging.exception("\n\n\n\n---------Ocorreu um erro no script de teste!!---------\n\n\n\n")
        # enviaEmail('<h2>Ocorreu um erro no scpripts de teste<h2>', '')
        sleep(3600)

while(1 > 0):
    rotina()