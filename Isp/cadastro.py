from warnings import catch_warnings
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep
from fordev.generators import people, uf, cnpj as generateCNPJ, state_registration as generateIE
import XPath.xPathErros as xPathErros
import XPath.xPathCadastro as xPathCadastro
import erros.VerificaErros as verificaErros
import SendEmail

def fechaPopUpSeAbrir(browser):
    if (len(browser.find_elements_by_xpath(xPathErros.popUp)) > 0):
        sleep(20)
        Clique = browser.find_element_by_xpath(xPathErros.botaoFechar)
        sleep(2)
        Clique.click()
    else:
        return    



def realizarCadastro():
    
    PATH = "../chromedriver"
    browser = webdriver.Chrome(PATH)
    url = "https://next.ispsaude.com.br//"
    browser.get(url)
    estado = uf()
    pessoa = people(uf_code=estado[0], sex='F', formatting=False)
    Telefone = pessoa.get('celular').split()
    Telefone = Telefone[0]
    ddd = Telefone[0:2]
    numero = Telefone[1:12]
    CPF = pessoa.get('cpf').split()
    CPF = CPF[0]
    
    Entre = browser.find_element_by_xpath(xPathCadastro.signUp)
    Entre.click()
    sleep(3.5)

    fechaPopUpSeAbrir(browser)

    Email = browser.find_element_by_xpath(xPathCadastro.cadEmail)
    sleep(0.3)
    Email.send_keys(pessoa.get('email'))
    sleep(1)
    
    fechaPopUpSeAbrir(browser)

    Senha = browser.find_element_by_xpath(xPathCadastro.cadSenha)
    sleep(0.3)
    Senha.send_keys(pessoa.get('senha'))
    #Senha.send_keys('a')
    
    sleep(0.5)
    mostrarSenha = browser.find_element_by_xpath(xPathCadastro.showSigninPasswordCad)
    mostrarSenha.click()
    sleep(1)
    mostrarSenha.click()
    sleep(1)

    fechaPopUpSeAbrir(browser)

    DDD = browser.find_element_by_xpath(xPathCadastro.cadDDD)
    sleep(0.3)
    DDD.send_keys(ddd)
    sleep(1)

    fechaPopUpSeAbrir(browser)

    Telefone = browser.find_element_by_xpath(xPathCadastro.cadTelefone)
    sleep(0.3)
    Telefone.send_keys(numero)
    sleep(1)


    fechaPopUpSeAbrir(browser)

    Nome = browser.find_element_by_xpath(xPathCadastro.cadNome)
    sleep(0.3)
    Nome.send_keys(pessoa.get('nome'))
    sleep(1)
    
    fechaPopUpSeAbrir(browser)

    Cpf = browser.find_element_by_xpath(xPathCadastro.cpf)
    for x in range(11):
        Cpf.send_keys(Keys.BACKSPACE)
        Cpf.send_keys(CPF[x])
    sleep(1)

    fechaPopUpSeAbrir(browser)
        
    Data = browser.find_element_by_xpath(xPathCadastro.cadData)
    for y in range(8):
        Data.send_keys(Keys.BACKSPACE)
    Data.send_keys(pessoa.get('data_nasc'))
    sleep(1)

    fechaPopUpSeAbrir(browser)
        
    Cadastrar = browser.find_element_by_xpath(xPathCadastro.cadastrar)
    Cadastrar.click()

    if (verificaErros.is_erro):
        SendEmail.enviaEmail()

    sleep(2)
    browser.quit()
    
try:
    while 1 > 0:
        realizarCadastro()
finally:
    sleep(2)



